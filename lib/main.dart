// ignore_for_file: unused_import

import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import './widget/DrawerWidget.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'มหาวิทยาลัยบูรพา',
      home: MyHomePage(title: 'มหาวิทยาลัยบูรพา'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [DeviceSection()],
      builder: (context) {
        return MaterialApp(
          useInheritedMediaQuery: true,
          debugShowCheckedModeBanner: false,
          title: 'ระบบบริการศึกษา มหาวิทยาลัยบูรพา',
          theme: ThemeData(
              appBarTheme:
                  const AppBarTheme(color: Color.fromARGB(255, 255, 201, 83))),
          home: Scaffold(
            backgroundColor: const Color.fromARGB(255, 255, 254, 251),
            appBar: AppBar(title: Text(title)),
            body: buildBody(),
            drawer: buildDrawer(context),
          ),
        );
      },
    );
  }
}

Widget buildBody() {
  return ListView(
    children: <Widget>[
      Container(
        color: Colors.black12,
        padding: const EdgeInsets.fromLTRB(60.0, 10.0, 10.0, 10.0),
        child: const Text(
          "ประกาศเรื่อง",
          style: TextStyle(
              fontSize: 25, color: Colors.black, fontWeight: FontWeight.w400),
        ),
      ),
      Container(
        padding: const EdgeInsets.fromLTRB(30.0, 10.0, 10.0, 0.0),
        child: const Text(
          "1. แบบประเมินความคิดเห็นของนักเรียนและนิสิตต่อการให้บริการของสำนักงานอธิการบดี(ด่วนที่สุด)",
          style: TextStyle(
              fontSize: 18, color: Colors.black, fontWeight: FontWeight.w400),
        ),
      ),
      Container(
        padding: const EdgeInsets.fromLTRB(30.0, 5.0, 10.0, 10.0),
        child: const Text(
          "   ขอเชิญนิสิตร่วมทำแบบประเมินความคิดเห็นของนิสิตต่อการให้บริการของสำนักงานอธิการบดี",
          style: TextStyle(
              fontSize: 16,
              color: Color.fromARGB(255, 255, 0, 0),
              fontWeight: FontWeight.w400),
        ),
      ),
      const Divider(),
      Container(
        padding: const EdgeInsets.fromLTRB(30.0, 10.0, 10.0, 0.0),
        child: const Text(
          "2. การทำบัตรนิสิตกับธนาคารกรุงไทย",
          style: TextStyle(
              fontSize: 18, color: Colors.black, fontWeight: FontWeight.w400),
        ),
      ),
      Container(
        padding: const EdgeInsets.fromLTRB(30.0, 5.0, 10.0, 10.0),
        child: const Text(
          "   กรณีบัตรหายเสียค่าใช้จ่ายในการทำ 100 บาท \n\n   นิสิตรหัส 65 วิทยาเขตบางแสนที่เข้าภาคเรียนที่ 1 ที่ยังไม่รับบัตรให้ติดต่อรับบัตรนิสิตที่ธนาคารกรุงไทย สาขา ม.บูรพา \n\n   นิสิตที่เข้าภาคเรียนที่ 2/2565 รอกำหนดการอีกครั้ง",
          style: TextStyle(
              fontSize: 16,
              color: Color.fromARGB(255, 255, 0, 0),
              fontWeight: FontWeight.w400),
        ),
      ),
      const Divider(),
    ],
  );
}
