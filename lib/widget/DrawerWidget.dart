// ignore_for_file: file_names

import 'package:flutter/material.dart';
import '../page/ProfilePage.dart';
import '../page/StudyExamTable.dart';

Drawer buildDrawer(BuildContext context) {
  return Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: [
        const UserAccountsDrawerHeader(
          decoration: BoxDecoration(color: Color.fromARGB(255, 255, 201, 83)),
          accountEmail: Text("63160208"),
          accountName: Text("Patchara Lamyaihwan"),
          currentAccountPicture: CircleAvatar(
            radius: 60.0,
            backgroundImage: NetworkImage(
                "https://gitlab.com/webprogramming11/lab4/-/raw/main/manami.png"),
          ),
        ),
        for (var lst in list)
          buildDrawerItem(
            txt: lst.toString(),
            onClick: (() => selectedItem(context, lst.toString())),
          )
      ],
    ),
  );
}

void selectedItem(BuildContext context, String index) {
  switch (index) {
    case 'ข้อมูลนิสิต':
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => const ProfilePage(),
        ),
      );
      break;
    case 'ตารางเรียน/สอบ':
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => const StudyExamTable(),
        ),
      );
      break;
  }
}

List<String> list = ['ข้อมูลนิสิต', 'ตารางเรียน/สอบ'];

Widget buildDrawerItem({required String txt, VoidCallback? onClick}) {
  return ListTile(
    title: Text(txt),
    onTap: onClick,
  );
}
