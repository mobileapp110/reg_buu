// ignore_for_file: unused_import, file_names
import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

class StudyExamTable extends StatelessWidget {
  const StudyExamTable({super.key});

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [DeviceSection()],
      builder: (context) {
        return MaterialApp(
          useInheritedMediaQuery: true,
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
              appBarTheme:
                  const AppBarTheme(color: Color.fromARGB(255, 255, 201, 83))),
          title: 'ระบบบริการศึกษา มหาวิทยาลัยบูรพา',
          home: Scaffold(
            backgroundColor: const Color.fromARGB(255, 255, 254, 251),
            appBar: AppBar(
                leading: IconButton(
                    onPressed: () => Navigator.pop(context),
                    icon: const Icon(Icons.arrow_back_ios)),
                title: const Text('ตารางเรียน/สอบ')),
            body: ListView(
              children: [
                Container(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 60),
                  child: studyTable(),
                ),
                const Divider(),
                Container(
                  child: examMidTable(),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 50),
                  child: examFinalTable(),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

Widget studyTable() {
  return Column(
    children: [
      Align(
        alignment: Alignment.centerLeft,
        child: Container(
          padding: const EdgeInsets.fromLTRB(50, 20, 30, 15),
          child: const Text(
            'อาจารย์ที่ปรึกษา: ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน, อาจารย์ภูสิต กุลเกษม',
            style: TextStyle(fontSize: 20),
          ),
        ),
      ),
      Align(
        alignment: Alignment.centerLeft,
        child: Container(
          padding: const EdgeInsets.fromLTRB(80, 20, 10, 10),
          child: const Text(
            'ตารางเรียนปีการศึกษา 2/2565',
            style: TextStyle(fontSize: 18),
          ),
        ),
      ),
      SizedBox(
        child: Card(
          color: const Color.fromARGB(188, 243, 243, 243),
          child: DataTable(
            columns: const [
              DataColumn(
                label: Text('Day/Time'),
              ),
              DataColumn(
                label: Text('9:00-10:00'),
              ),
              DataColumn(
                label: Text('10:00-12:00'),
              ),
              DataColumn(
                label: Text('13:00-15:00'),
              ),
              DataColumn(
                label: Text('15:00-17:00'),
              ),
              DataColumn(
                label: Text('17:00-19:00'),
              ),
            ],
            rows: const [
              DataRow(
                cells: [
                  DataCell(Text('จันทร์')),
                  DataCell(Text('')),
                  DataCell(Text('88624559-59\n  IF-4M210IF')),
                  DataCell(Text('88624459-59\n IF-3M210IF')),
                  DataCell(Text('')),
                  DataCell(Text('88624359-59\n IF-3M210IF')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('อังคาร')),
                  DataCell(Text('')),
                  DataCell(Text('88634259-59\n  IF-4C02IF')),
                  DataCell(Text('88624559-59\n IF-3C03IF')),
                  DataCell(Text('')),
                  DataCell(Text('88624459-59\n IF-4C01IF')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('พุธ')),
                  DataCell(Text('')),
                  DataCell(Text('88634459-59\n  IF-4C02IF')),
                  DataCell(Text('88634259-59\n IF-4C01IF')),
                  DataCell(Text('88624359-59\n IF-3C01IF')),
                  DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('พฤหัสบดี')),
                  DataCell(Text('')),
                  DataCell(Text('')),
                  DataCell(Text('')),
                  DataCell(Text('')),
                  DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('ศุกร์')),
                  DataCell(Text('88646259-59\n  IF-5T05IF')),
                  DataCell(Text('88646259-59\n  IF-5T05IF')),
                  DataCell(Text('88634459-59\n IF-4C02IF')),
                  DataCell(Text('')),
                  DataCell(Text('')),
                ],
              ),
            ],
          ),
        ),
      )
    ],
  );
}

Widget examMidTable() {
  return Column(
    children: [
      Align(
        alignment: Alignment.centerLeft,
        child: Container(
          padding: const EdgeInsets.fromLTRB(80, 50, 10, 10),
          child: const Text(
            'ตารางสอบกลางภาค',
            style: TextStyle(fontSize: 18),
          ),
        ),
      ),
      DataTable(
        columns: const [
          DataColumn(
            label: Text('Date/Time'),
          ),
          DataColumn(
            label: Text('9:00-12:00'),
          ),
          DataColumn(
            label: Text('12:00-13:00'),
          ),
          DataColumn(
            label: Text('13:00-16:00'),
          ),
          DataColumn(
            label: Text('17:00-20:00'),
          ),
        ],
        rows: const [
          DataRow(
            cells: [
              DataCell(Text('Mon\n16/01/66')),
              DataCell(Text('88624559-59\nSoftware Testing')),
              DataCell(Text('')),
              DataCell(Text('')),
              DataCell(Text('88624359-59\nWeb Programming')),
            ],
          ),
          DataRow(
            cells: [
              DataCell(Text('Tue\n17/01/66')),
              DataCell(Text('')),
              DataCell(Text('')),
              DataCell(Text('')),
              DataCell(Text('88624459-59\nOOAD')),
            ],
          ),
          DataRow(
            cells: [
              DataCell(Text('Wed\n18/01/66')),
              DataCell(Text('')),
              DataCell(Text('')),
              DataCell(Text('88634259-59\nMultimedia')),
              DataCell(Text('88646259-59\n NLP')),
            ],
          ),
          DataRow(
            cells: [
              DataCell(Text('Thu\n19/01/66')),
              DataCell(Text('')),
              DataCell(Text('')),
              DataCell(Text('')),
              DataCell(Text('')),
            ],
          ),
          DataRow(
            cells: [
              DataCell(Text('Fri\n20/01/66')),
              DataCell(Text('')),
              DataCell(Text('')),
              DataCell(Text('88634459-59\nMobile')),
              DataCell(Text('')),
            ],
          ),
        ],
      ),
    ],
  );
}

Widget examFinalTable() {
  return Column(
    children: [
      Align(
        alignment: Alignment.centerLeft,
        child: Container(
          padding: const EdgeInsets.fromLTRB(80, 50, 10, 10),
          child: const Text(
            'ตารางสอบปลายภาค',
            style: TextStyle(fontSize: 18),
          ),
        ),
      ),
      DataTable(
        columns: const [
          DataColumn(
            label: Text('Date/Time'),
          ),
          DataColumn(
            label: Text('9:00-12:00'),
          ),
          DataColumn(
            label: Text('12:00-13:00'),
          ),
          DataColumn(
            label: Text('13:00-16:00'),
          ),
          DataColumn(
            label: Text('17:00-20:00'),
          ),
        ],
        rows: const [
          DataRow(
            cells: [
              DataCell(Text('Mon\n27/03/66')),
              DataCell(Text('88624559-59\nSoftware Testing')),
              DataCell(Text('')),
              DataCell(Text('')),
              DataCell(Text('88624359-59\nWeb Programming')),
            ],
          ),
          DataRow(
            cells: [
              DataCell(Text('Tue\n28/03/66')),
              DataCell(Text('')),
              DataCell(Text('')),
              DataCell(Text('')),
              DataCell(Text('88624459-59\nOOAD')),
            ],
          ),
          DataRow(
            cells: [
              DataCell(Text('Wed\n29/03/66')),
              DataCell(Text('88634459-59\nMobile')),
              DataCell(Text('')),
              DataCell(Text('88634259-59\nMultimedia')),
              DataCell(Text('')),
            ],
          ),
          DataRow(
            cells: [
              DataCell(Text('Thu\n30/03/66')),
              DataCell(Text('')),
              DataCell(Text('')),
              DataCell(Text('')),
              DataCell(Text('')),
            ],
          ),
          DataRow(
            cells: [
              DataCell(Text('Fri\n31/03/66')),
              DataCell(Text('88646259-59\nNLP')),
              DataCell(Text('')),
              DataCell(Text('')),
              DataCell(Text('')),
            ],
          ),
        ],
      ),
    ],
  );
}
