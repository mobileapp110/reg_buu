// ignore_for_file: unused_import, file_names, non_constant_identifier_names
import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [DeviceSection()],
      builder: (context) {
        return MaterialApp(
          useInheritedMediaQuery: true,
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            appBarTheme: const AppBarTheme(
              color: Color.fromARGB(255, 255, 201, 83),
            ),
          ),
          title: 'ระบบบริการศึกษา มหาวิทยาลัยบูรพา',
          home: Scaffold(
            backgroundColor: const Color.fromARGB(255, 255, 254, 251),
            appBar: AppBar(
              leading: IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: const Icon(Icons.arrow_back_ios)),
              title: const Text('ข้อมูลนิสิต'),
            ),
            body: ListView(
              children: [
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 60),
                  child: CardInfo(),
                ),
                const Divider(),
                Container(
                  child: CardGrade12565(),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 50),
                  child: CardGrade22565(),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

Widget CardInfo() {
  return Column(
    children: [
      Align(
        alignment: Alignment.centerLeft,
        child: Container(
          padding: const EdgeInsets.fromLTRB(50, 15, 30, 15),
          child: const Text(
            'ข้อมูลด้านการศึกษา',
            style: TextStyle(fontSize: 20),
          ),
        ),
      ),
      SizedBox(
        width: 800,
        child: Card(
          color: const Color.fromARGB(188, 243, 243, 243),
          child: DataTable(
            columns: const [
              DataColumn(
                label: Text('ชื่อ:    นางสาวพัชรา ลำใยหวาน'),
              ),
              DataColumn(
                label: Text(
                  'รหัสประจำตัว:   63160208',
                ),
              ),
            ],
            rows: const [
              DataRow(
                cells: [
                  DataCell(Text('คณะ:   คณะวิทยาการสารสนเทศ')),
                  DataCell(Text('วิทยาเขต:   บางแสน')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('สถานภาพ:   กำลังศึกษา')),
                  DataCell(Text('')),
                ],
              ),
            ],
          ),
        ),
      )
    ],
  );
}

Widget CardGrade12565() {
  return Column(
    children: [
      Align(
        alignment: Alignment.centerLeft,
        child: Container(
          padding: const EdgeInsets.fromLTRB(50, 50, 30, 15),
          child: const Text(
            'ผลการศึกษา',
            style: TextStyle(fontSize: 20),
          ),
        ),
      ),
      Align(
        alignment: Alignment.centerLeft,
        child: Container(
          padding: const EdgeInsets.fromLTRB(80, 15, 30, 10),
          child: const Text(
            'ปีการศึกษา:1/2565',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      SizedBox(
        width: 800,
        child: Card(
          color: const Color.fromARGB(188, 243, 243, 243),
          child: DataTable(
            columns: const [
              DataColumn(
                label: Text('รหัสวิชา'),
              ),
              DataColumn(
                label: Text('ชื่อรายวิชา'),
              ),
              DataColumn(
                label: Text('หน่วยกิต'),
              ),
              DataColumn(
                label: Text('เกรด'),
              ),
            ],
            rows: const [
              DataRow(
                cells: [
                  DataCell(Text('23529164')),
                  DataCell(Text('Chinese')),
                  DataCell(Text('3')),
                  DataCell(Text('A')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('88624259')),
                  DataCell(Text('Mobile')),
                  DataCell(Text('3')),
                  DataCell(Text('A')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('88631159')),
                  DataCell(Text('Algorithm')),
                  DataCell(Text('3')),
                  DataCell(Text('A')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('88633159')),
                  DataCell(Text('ComNet')),
                  DataCell(Text('3')),
                  DataCell(Text('A')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('88634159')),
                  DataCell(Text('SoftDev')),
                  DataCell(Text('3')),
                  DataCell(Text('A')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('88635359')),
                  DataCell(Text('UX-UI')),
                  DataCell(Text('3')),
                  DataCell(Text('A')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('88636159')),
                  DataCell(Text('AI')),
                  DataCell(Text('3')),
                  DataCell(Text('A')),
                ],
              ),
            ],
          ),
        ),
      )
    ],
  );
}

Widget CardGrade22565() {
  return Column(
    children: [
      Align(
        alignment: Alignment.centerLeft,
        child: Container(
          padding: const EdgeInsets.fromLTRB(80, 20, 30, 10),
          child: const Text(
            'ปีการศึกษา:2/2565',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      SizedBox(
        width: 800,
        child: Card(
          color: const Color.fromARGB(188, 243, 243, 243),
          child: DataTable(
            columns: const [
              DataColumn(
                label: Text('รหัสวิชา'),
              ),
              DataColumn(
                label: Text('ชื่อรายวิชา'),
              ),
              DataColumn(
                label: Text('หน่วยกิต'),
              ),
              DataColumn(
                label: Text('เกรด'),
              ),
            ],
            rows: const [
              DataRow(
                cells: [
                  DataCell(Text('88624359')),
                  DataCell(Text('Web')),
                  DataCell(Text('3')),
                  DataCell(Text('A')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('88624459')),
                  DataCell(Text('OOAD')),
                  DataCell(Text('3')),
                  DataCell(Text('A')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('88624559')),
                  DataCell(Text('SoftTest')),
                  DataCell(Text('3')),
                  DataCell(Text('A')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('88634259')),
                  DataCell(Text('Multimedia')),
                  DataCell(Text('3')),
                  DataCell(Text('A')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('88634459')),
                  DataCell(Text('Mobile')),
                  DataCell(Text('3')),
                  DataCell(Text('A')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('88646259')),
                  DataCell(Text('NLP')),
                  DataCell(Text('3')),
                  DataCell(Text('A')),
                ],
              ),
            ],
          ),
        ),
      )
    ],
  );
}
